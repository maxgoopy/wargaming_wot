#include <cassert>

#include "Ship.h"
#include "Params.h"
#include "Utils/Math.h"
#include "Aircraft/Aircraft.h"


Ship::Ship() :
	mesh(nullptr)
{
	rechargingTime = params::ship::AIRCRAFT_RECHARGING_TIME;
}


void Ship::init()
{
	assert(!mesh);
	mesh = scene::createShipMesh();
	position = Vector2(0.f, 0.f);
	angle = 0.f;
	for (bool& key : input)
		key = false;

	//Init aircrafts
	for (int i = 0; i < params::ship::DEFAULT_AIRCRAFTS_COUNT; ++i)
	{
		auto aircraft = new Aircraft;
		aircraft->setSimulatePhysics(false);
		aircraft->activateController(false);
		aircrafts.push_back(aircraft);
		launchQueue.push(aircraft);
	}
}


void Ship::deinit()
{
	//Destroy mesh
	scene::destroyMesh(mesh);
	mesh = nullptr;

	//Place Goal marker in default position
	scene::placeGoalMarker(0, 0);

	//Delete all aircrafts and clear containers
	runwayFree(runwayClient);
	for (auto aircraft : aircrafts)
		delete aircraft;

	aircrafts.clear();

	rechargingAircrafts.clear();

	std::queue<Aircraft*> empty;
	std::swap(launchQueue, empty);

	//Set variables in default
	runwayClient = nullptr;
	launchingAircraftProgress = 0;
	landingAircraftProgress = 0;
	landingInProgress = false;
	launchInProgress = false;
}


void Ship::update(float dt)
{
	float linearSpeed = 0.f;
	float angularSpeed = 0.f;

	if (input[game::KEY_FORWARD])
	{
		linearSpeed = params::ship::LINEAR_SPEED;
	}
	else if (input[game::KEY_BACKWARD])
	{
		linearSpeed = -params::ship::LINEAR_SPEED;
	}

	if (input[game::KEY_LEFT] && linearSpeed != 0.f)
	{
		angularSpeed = params::ship::ANGULAR_SPEED;
	}
	else if (input[game::KEY_RIGHT] && linearSpeed != 0.f)
	{
		angularSpeed = -params::ship::ANGULAR_SPEED;
	}

	angle = angle + angularSpeed * dt;
	position = position + linearSpeed * dt * Vector2(std::cos(angle), std::sin(angle));
	scene::placeMesh(mesh, position.x, position.y, angle);

	//Update aircrafts
	for (auto aircraft : aircrafts)
		aircraft->update(dt);

	//Call other update functions
	updateLaunchingAircraft(dt);
	updateLandingAircraft(dt);
	updateRechargingAircrafts(dt);
}


void Ship::keyPressed(int key)
{
	assert(key >= 0 && key < game::KEY_COUNT);
	input[key] = true;
}


void Ship::keyReleased(int key)
{
	assert(key >= 0 && key < game::KEY_COUNT);
	input[key] = false;
}


void Ship::mouseClicked(Vector2 worldPosition, bool isLeftButton)
{
	if (isLeftButton)
	{
		scene::placeGoalMarker(worldPosition.x, worldPosition.y);

		//Set target for all aircrafts
		for (auto iter : aircrafts)
			iter->setGoalMarker(worldPosition);
	}
	else
	{
		tryLaunchAircraft();
	}
}

bool Ship::runwayReserve(Aircraft* aircraft)
{
	if (aircraft == nullptr)
		return false;

	if (runwayClient == nullptr)
	{
		runwayClient = aircraft;
		return true;
	}

	return runwayClient == aircraft;
}

void Ship::runwayFree(Aircraft* aircraft)
{
	if (runwayClient == aircraft)
		runwayClient = nullptr;
}

void Ship::runwayLandingPosition(Vector2& outPoint, float& outAngle)
{
	outPoint = getAircraftFinPoint();
	outAngle = angle + 3.14f;
}

void Ship::initLanding(Aircraft* aircraft)
{
	//Try get controll on the aircraft
	if (!getControlUnderAircraft(aircraft))
		return;

	//If controll granted start landing logic
	landingInProgress = true;
	runwayClient->setAngle(angle + 3.14f);
	runwayClient->setPoint(getAircraftFinPoint());

}

Vector2 Ship::getAircraftStartPoint(bool globalSpace) const
{
	if (globalSpace)
		return position + aircraftStartPoint.rotate(angle);

	return aircraftStartPoint;
}

Vector2 Ship::getAircraftFinPoint(bool globalSpace) const
{
	if (globalSpace)
		return position + aircraftFinPoint.rotate(angle);

	return aircraftFinPoint;
}

Vector2 Ship::getRunwayDirection()const
{
	return (getAircraftFinPoint() - getAircraftStartPoint()).normalize();
}

void Ship::tryLaunchAircraft()
{
	//Check if we have aircraft for launching and if runway reserved
	if (launchQueue.empty() || runwayClient != nullptr)
		return;

	//Initialize launching aircraft and remove them from queue
	launchQueue.front()->init(this);
	initLaunchingAircraft(launchQueue.front());
	launchQueue.pop();
}

void Ship::initLaunchingAircraft(Aircraft* aircraft)
{
	//Try get control on the aircraft
	if (!getControlUnderAircraft(aircraft))
		return;

	//If control granted start launching logic
	launchInProgress = true;
	runwayClient->setPoint(getAircraftStartPoint(true));
	runwayClient->setAngle(angle);
	runwayClient->setFlightTime(params::aircraft::FLIGHT_TIME);
}

void Ship::updateLaunchingAircraft(float dt)
{
	if (!launchInProgress)
		return;

	launchingAircraftProgress += dt / launchingAircraftTime;

	//Calculate new point for aircraft according to launching progress
	Vector2 newPoint = Math::scale(
		(float)pow(launchingAircraftProgress, 3),
		0.f,
		1.f,
		getAircraftStartPoint(),
		getAircraftFinPoint()
	);

	//Set new aircraft position
	runwayClient->setAngle(angle);
	runwayClient->setPoint(newPoint);

	//If launching finished set default velocity and free aircraft
	if (launchingAircraftProgress >= 1)
	{
		runwayClient->setVelocity(params::aircraft::LINEAR_SPEED * getRunwayDirection());
		runwayClient->setAngularVelocity(0);
		runwayClient->setSimulatePhysics(true);
		runwayClient->activateController(true);
		launchInProgress = false;
		runwayFree(runwayClient);
		launchingAircraftProgress = 0;
	}
}

void Ship::updateLandingAircraft(float dt)
{
	if (!landingInProgress)
		return;

	landingAircraftProgress += dt / landingAircraftTime;

	//Calculate new point for aircraft according to launching progress
	Vector2 newPosition = Math::scale(
		(float)pow(landingAircraftProgress, 0.5),
		0.f,
		1.f,
		getAircraftFinPoint(),
		getAircraftStartPoint()
	);

	//Set new aircraft position
	runwayClient->setAngle(angle + 3.14f);
	runwayClient->setPoint(newPosition);

	//Landing finished
	if (landingAircraftProgress >= 1)
	{
		//Set zero velocity and deinit aircraft
		runwayClient->setVelocity(Vector2::ZERO_VECTOR);
		runwayClient->setAngularVelocity(0);
		runwayClient->deinit();

		//Add aircraft for recharging and free Runway
		landingInProgress = false;
		rechargingAircrafts.push_back({ runwayClient, 0 });
		runwayFree(runwayClient);
		landingAircraftProgress = 0;
	}
}

void Ship::updateRechargingAircrafts(float dt)
{
	if (dt <= 0)
		return;

	//Increase aircrafts recharging time and push them in launching queue when recharging complete
	for (auto iter = rechargingAircrafts.begin(); iter!=rechargingAircrafts.end();)
	{
		iter->time += dt;
		if (iter->time >= rechargingTime)
		{
			launchQueue.push(iter->aircraft);
			rechargingAircrafts.erase(iter++);
		}
		else
		{
			++iter;
		}
	}
}

bool Ship::getControlUnderAircraft(Aircraft* aircraft)
{
	//Try reserve Runway
	if (!runwayReserve(aircraft))
		return false;

	//If saccess get control on the aircraft
	runwayClient->setSimulatePhysics(false);
	runwayClient->activateController(false);
	return true;
}
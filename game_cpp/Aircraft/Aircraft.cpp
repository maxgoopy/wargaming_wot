#include <cmath>

#include "Aircraft.h"
#include "..\..\framework\scene.hpp"
#include "..\..\framework\game.hpp"
#include "AircraftAerodynamics.h"
#include "AircraftEngine.h"
#include "AircraftController_Engine.h"
#include "AircraftController.h"
#include "..\Params.h"

void Aircraft::ApplyImpulse()
{
	velocity += impulse;
	impulse.x = 0;
	impulse.y = 0;
}

void Aircraft::ApplyAngularImpulse()
{
	angular_velocity += angular_impulse;
	angular_impulse = 0;
}

void Aircraft::update_physics(float dt)
{
	if (dt <= 0 || !simulatePhysics)
		return;

	//First update physics component
	aircraftEngine->update(dt);
	aircraftAerodynamics->update(dt);

	//Apply impulse
	ApplyImpulse();
	ApplyAngularImpulse();

	//Simple integration
	point = point + dt * velocity;
	angle += dt * angular_velocity;
	angle = angleBetwenVectors(Vector2(1, 0), getForwardVector());

	//Simple dissipation
	velocity *= std::fmax(0, 1 - linearDamping * dt);
	angular_velocity *= std::fmax(0, 1 - angularDamping * dt);

	//Check restriction
	if (velocity.length() > 2 * params::aircraft::LINEAR_SPEED)
		velocity *= 2 * params::aircraft::LINEAR_SPEED / velocity.length();

	angular_velocity = std::fmax(-2 * params::aircraft::ANGULAR_SPEED, std::fmin(2 * params::aircraft::ANGULAR_SPEED, angular_velocity));

	//Calculate often used variables
	forwardVelocity = dotProduct(velocity, getForwardVector());
}

Aircraft::Aircraft()
{
	//Init aircraft aerodynamics
	aircraftAerodynamics = new AircraftAerodynamics(this);

	//Init aircraft engine whith hard restrictions
	aircraftEngine = new AircraftEngine(
		this,
		2*params::aircraft::LINEAR_SPEED,
		2*params::aircraft::ANGULAR_SPEED
		);

	//Init engine controller
	controller_Engine = new AircraftController_Engine(this, aircraftEngine);
	controller_Engine->setMaxAngularSpeed(params::aircraft::ANGULAR_SPEED);		//hard restriction for controller
	controller_Engine->setMaxLinearSpeed(1.5*params::aircraft::LINEAR_SPEED);	//hard restriction for controller
	controller_Engine->setMinFlightSpeed(0.3 * params::aircraft::LINEAR_SPEED);	//soft restriction for min speed
	controller_Engine->setMaxFlightSpeed(params::aircraft::LINEAR_SPEED);		//soft restriction for target flight speed

	/*Set what use as controllable variable for angular controller
	* true - aircraft velocity will be used as controllable variable
	* It include active traverse velocity dissipation by controller
	* false - aircraft forward vector will be used as controllable variable
	* Travers velocity will be dissipated passivly by aircraft aerodinamics
	*/
	controller_Engine->setCalculateTargetAngleByVelocity(true);

	//Init controller
	controller = new AircraftController(this);
}


Aircraft::~Aircraft()
{
	deinit();

	delete aircraftAerodynamics;
	delete aircraftEngine;
	delete controller_Engine;
}

void Aircraft::init(IRunway* ship)
{
	if (mesh == nullptr)
		mesh = scene::createAircraftMesh();
	
	runway = ship;
}


void Aircraft::deinit()
{
	runway = nullptr;
	if (mesh != nullptr)
	{
		scene::destroyMesh(mesh);
		mesh = nullptr;
	}

}

void Aircraft::update(float dt)
{
	controller->update(dt);
	controller_Engine->update(dt);

	update_physics(dt);

	//Last action - update mesh on the map
	if(mesh!=nullptr)
		scene::placeMesh(mesh, point.x, point.y, angle);
}

void Aircraft::addImpulse(const Vector2& impulse)
{
	this->impulse += impulse;
}

Vector2 Aircraft::getForwardVector() const
{
	return Vector2(cos(angle),sin(angle));
}


void Aircraft::setGoalMarker(const Vector2& goal)
{
	goalMarker = goal;
	controller->setTargetPoint(goal);
}

void Aircraft::activateController(bool active)
{
	controller_Engine->setActive(active);
	controller->setActive(active);
}

void Aircraft::setFlightTime(float time)
{
	controller->setFlightTime(time);
}


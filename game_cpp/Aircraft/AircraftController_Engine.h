#pragma once
#include "..\Utils\Vector2.h"
#include "..\Utils\PID.h"

class Aircraft;
class AircraftEngine;

//Controller class for AircraftEngine. Basicaly it's simple PID
class AircraftController_Engine
{
	//Controlled Aircraft
	Aircraft* aircraft = nullptr;
	//Controlled engine
	AircraftEngine* engine = nullptr;

	//Target point for contoller
	Vector2 targetPoint;

	//PID controllers for Moment and Force
	PID* controllerMoment = nullptr;
	PID* controllerForce = nullptr;

	//Is controller activated
	bool active = false;

	//Hard restriction. Max aircraft angular speed. If <0 then no restriction
	float maxAngularSpeed = -1;
	
	//Hard restriction. Maximum aircraft speed. If <0 then no restriction
	float maxLinearSpeed = -1;

	//Soft restriction. Minimal required aircraft speed 
	float minFlightSpeed = 0;

	//Soft restriction. Maximum/work aircraft flight speed
	float maxFlightSpeed = 1;

	//Use aircraft velocity direction as state for controller
	bool targetAngleByVelocity = true;

	AircraftController_Engine();
	

	float calculateMomentControl(float dt);
	float calculateForceControl(float dt);
	float calculateTargetAngle();
	float recalculateMomentControlWithRestrictions(float control);
	float recalculateForceControlWithRestrictions(float control);

public:
	AircraftController_Engine(Aircraft* aircraft, AircraftEngine* engine);
	~AircraftController_Engine();

	//Main update function
	void update(float dt);

	//Set target point for controller
	void setTargetPoint(const Vector2& point) { targetPoint = point; };

	//Activate/Deactivate controller
	void setActive(bool active);

	//Hard restriction. Max aircraft angular speed. If <0 then no restriction
	void setMaxAngularSpeed(float max) { maxAngularSpeed = max; };

	//Hard restriction. Maximum aircraft speed. If <0 then no restriction
	void setMaxLinearSpeed(float max) { maxLinearSpeed = max; };

	//Soft restriction. Minimal required aircraft speed 
	void setMinFlightSpeed(float min) { minFlightSpeed = min; };

	//Soft restriction. Maximum/work aircraft flight speed
	void setMaxFlightSpeed(float max) { maxFlightSpeed = max; };

	//Use aircraft velocity direction(true) or aircraft forward vector(false) as state for moment controller   
	void setCalculateTargetAngleByVelocity(bool b) { targetAngleByVelocity = b; };
};


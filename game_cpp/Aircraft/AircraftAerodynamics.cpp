#include "AircraftAerodynamics.h"
#include <cmath>
#include <cassert>

Vector2 AircraftAerodynamics::calculateImpulse(float dt)
{
	Vector2 velocity = craft->getVelocity();
	Vector2 forwardVelocity = craft->getForwardVelocity() * craft->getForwardVector();
	Vector2 lateralVelocity = velocity - forwardVelocity;

	Vector2 dissipationImpulse = std::fmin(1,dissipationCoef * dt) * lateralVelocity;
	
	//Calculate velocity after our dissipation
	velocity = velocity - dissipationImpulse;

	//Calculate total coef for transfer impulse
	float coef = std::fmin(1, calculateTransferEfficiency() * transferCoef * dt);

	auto transferImpulse = velocity - velocity.rotate(coef * angleBetwenVectors(velocity, craft->getForwardVector()));

	//auto totalImpulse = -(dissipationImpulse + transferImpulse) ;

	return -(dissipationImpulse + transferImpulse);
}

float AircraftAerodynamics::calculateTransferEfficiency()
{
	auto angleOfAttack = angleBetwenVectors(craft->getForwardVector(), craft->getVelocity());

	/*Maximum efficiency if attack angle -> 0
	* Efficienty = 0 if attack angle > pi/2
	*/
	float efficiency = std::fmax(0, 1 - std::abs(angleOfAttack) / (3.14 / 2));
	return efficiency;
}

AircraftAerodynamics::AircraftAerodynamics(Craft* craft)
{
	assert(craft);
	this->craft = craft;
}

void AircraftAerodynamics::update(float dt)
{
	if (dt <= 0)
		return;
	craft->addImpulse(calculateImpulse(dt));
}

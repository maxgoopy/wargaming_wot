#pragma once
#include "IRunway.h"
#include "..\Utils\Vector2.h"

class Ship;
class AircraftEngine;
class AircraftController;
class AircraftController_Engine;
class AircraftAerodynamics;
namespace scene
{
	class Mesh;
}

/*Aircraft class*/
class Aircraft
{
	//Aircraft mesh
	scene::Mesh *mesh = nullptr;

	//Home runway
	IRunway *runway = nullptr;

	//Component for simulating aerodinamics
	AircraftAerodynamics* aircraftAerodynamics;

	//Component for add force and moment to aircraft
	AircraftEngine* aircraftEngine;

	//Main controller of aircraft
	AircraftController* controller;

	//Controller for aircraftEngine (simple movement controller)
	AircraftController_Engine* controller_Engine;

	//Goal point on the map for patrolling
	Vector2 goalMarker;

	//Simulate physics
	bool simulatePhysics = true;

	//Aircraft position, velocity and impulse
	Vector2 point;
	Vector2 velocity;
	Vector2 impulse;
	float angle;
	float angular_velocity;
	float angular_impulse;

	//Velocity projection on forward vector
	float forwardVelocity;
	
	//Damping parameters 
	float linearDamping = 0.1f;
	float angularDamping = 0.01f;

private:
	//Apply impulse to aicraft and empty them
	void ApplyImpulse();

	//Apply angular impulse to aicraft and empty them
	void ApplyAngularImpulse();

	//Physics update logic
	void update_physics(float dt);

public:
	Aircraft();
	~Aircraft();

	//Initialize aircraft, optionaly with given home runway
	void init(IRunway* runway = nullptr);

	//Deinitialize aircraft and delete mesh
	void deinit();

	//Main update function
	void update(float dt);

	void setPoint(const Vector2& point) { this->point = point; };
	void setAngle(float angle) { this->angle = angle; };
	void setVelocity(const Vector2& newVelocity) { velocity = newVelocity; };
	void setAngularVelocity(float newAngularVelocity) { angular_velocity = newAngularVelocity; };
	void setSimulatePhysics(bool simulate) { simulatePhysics = simulate; };

	//Set aircraft target for patrolling
	void setGoalMarker(const Vector2& goal);
	//Set aircraft time for flight
	void setFlightTime(float time);
	//Activate/deactivate aircraft controller
	void activateController(bool active);

	Vector2 getPoint() const { return point; };
	float getAngle() const { return angle; };
	Vector2 getVelocity() const { return velocity; };
	float getAngularVelocity() const { return angular_velocity; };
	//Get aircraft forward vector
	Vector2 getForwardVector() const;
	//Get ptrojection of velocity on forward vector
	float getForwardVelocity() const { return forwardVelocity; };
	//Get point for patrolling
	Vector2 getTargetPoint() const { return goalMarker; };
	//Get engine controller
	AircraftController_Engine* getControllerEngine() const { return controller_Engine; };
	//Get home runway
	IRunway* getRunway()const { return runway; };

	//Add Linear impulce to the aircraft
	void addImpulse(const Vector2& impulse) ;
	//Add angular impulse to the aircraft
	void addAngularImpulse(float impulse) { this->angular_impulse += impulse; };
	

};


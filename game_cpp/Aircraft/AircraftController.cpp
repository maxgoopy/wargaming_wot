#include "AircraftController.h"
#include "Aircraft.h"
#include "AircraftController_Engine.h"
#include <cassert>

AircraftController::AircraftController(Aircraft* aircraft)
{
	assert(aircraft);
	this->aircraft = aircraft;

	//Init path generator
	path = new AircraftController_Path();
}

void AircraftController::update(float dt)
{
	if (!active)
		return;

	updateFlightTime(dt);
	updateMoveToTarget(dt);
	updateMoveToRunway(dt);
}

Vector2 AircraftController::getTargetPointOnPath(const Vector2& currentPoint)
{
	return path->getTargetPointOnPath(currentPoint);
}

void AircraftController::updateFlightTime(float dt)
{
	if (dt <= 0)
		return;

	if(!moveToRunway)
		flightTime -= dt;

	//If flight time left try init move to runway
	if (flightTime <= 0)
		initMoveToRunway();

}

void AircraftController::updateMoveToTarget(float dt)
{
	if (dt <= 0 || moveToRunway)
		return;
	moveTo(getTargetPointOnPath(aircraft->getPoint()));
}

void AircraftController::updateMoveToRunway(float dt)
{
	if (dt <= 0 || !moveToRunway)
		return;
	
	//If reached runway landing point initialize landing
	if (isPointReached(runwayStartPoint, landingTolerance))
		aircraft->getRunway()->initLanding(aircraft);


	getRunwayStartPosition(runwayStartPoint, runwayAngle);
	path->createNewPath(runwayStartPoint);
	moveTo(getTargetPointOnPath(aircraft->getPoint()));
}

void AircraftController::moveTo(const Vector2& target)
{
	aircraft->getControllerEngine()->setTargetPoint(target);
}

bool AircraftController::isPointReached(const Vector2& point, float tolerance) const
{
	return (point-aircraft->getPoint()).length()<= tolerance;
}

void AircraftController::getRunwayStartPosition(Vector2& outPoint, float& outAngle) const
{
	if (aircraft->getRunway()!=nullptr)
		aircraft->getRunway()->runwayLandingPosition(outPoint, outAngle);
}

bool AircraftController::tryReserveRunway()
{
	return aircraft->getRunway()->runwayReserve(aircraft);
}

void AircraftController::setTargetPoint(const Vector2& target)
{
	targetPoint = target;

	//If note moving to runway recreate path
	if (!moveToRunway)
		path->createNewPath(targetPoint);
}

void AircraftController::initMoveToRunway()
{
	//If successfuly reserve runway then start move to runway logic
	if (tryReserveRunway())
		moveToRunway = true;
}

void AircraftController::setFlightTime(float time)
{
	flightTime = time;
	if (flightTime > 0)
	{
		moveToRunway = false;
		path->createNewPath(targetPoint);
	}
}

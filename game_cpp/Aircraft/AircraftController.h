#pragma once
#include "../Utils/Vector2.h"
#include "AircraftController_Path.h"

class Aircraft;

//Class controller for Aircraft
class AircraftController
{
	//Is controller active
	bool active = false;

	//Controlled aircraft
	Aircraft* aircraft;
	//Path generator
	AircraftController_Path* path;

	//Target point for patrolling
	Vector2 targetPoint;
	//Current target point on path
	Vector2 targetPointOnPath;
	
	//Raunway landing position
	Vector2 runwayStartPoint;
	float runwayAngle;

	//Left time for flight
	float flightTime = 0;
	//Is moved to Runway
	bool moveToRunway = false;
	//Landing tolerance for call initLanding 
	float landingTolerance = 0.5;

	AircraftController();

	//Get current target point on the path
	Vector2 getTargetPointOnPath(const Vector2& currentPoint);

	//Update flight time logic
	void updateFlightTime(float dt);

	//Update move to target logic
	void updateMoveToTarget(float dt);

	//Update move to runway logic
	void updateMoveToRunway(float dt);

	//Cammand for aircraft for simple moving to target
	void moveTo(const Vector2& target);

	//Is aircraft rached point whit given tolerance
	bool isPointReached(const Vector2& point, float tolerance = 0.5f) const;

	//Get landing position on Runway
	void getRunwayStartPosition(Vector2& outPoint, float& outAngle) const;

	//Try reserve Runway, return true on success
	bool tryReserveRunway();
	

public:
	AircraftController(Aircraft* aircraft);

	//Main update function
	void update(float dt);

	//Set point for patrolling
	void setTargetPoint(const Vector2& target);

	//Set left time for flight
	void setFlightTime(float time);

	//Activate/deactivate controller
	void setActive(bool active) { this->active = active; };

	//Initialize move to Runway
	void initMoveToRunway();
};


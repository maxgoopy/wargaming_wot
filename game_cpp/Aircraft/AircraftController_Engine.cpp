#include "AircraftController_Engine.h"
#include <cassert>
#include "Aircraft.h"
#include "AircraftEngine.h"
#include "..\Utils\PID_Limited.h"
#include "..\Utils\Math.h"

float AircraftController_Engine::calculateTargetAngle()
{
	if(targetAngleByVelocity)
		//Get angle betwen velocity and target point
		return  angleBetwenVectors(aircraft->getVelocity(), targetPoint - aircraft->getPoint());
	else
		//Get angle betwen forward vector and target point
		return  angleBetwenVectors(aircraft->getForwardVector(), targetPoint - aircraft->getPoint());
}

float AircraftController_Engine::recalculateMomentControlWithRestrictions(float control)
{
	if (maxAngularSpeed<0)
		return control;

	float modifyCoef = 1;

	//Calculate modifyCoef for slowly decrease control value if the angular velocity is approaching to critical
	if (control > 0)
		modifyCoef = Math::scale(
			aircraft->getAngularVelocity(),
			maxAngularSpeed * 0.95f,
			maxAngularSpeed * 1.05f,
			1.f,
			0.f
			);
	else
		modifyCoef = Math::scale(
			aircraft->getAngularVelocity(),
			-maxAngularSpeed * 1.05f,
			-maxAngularSpeed * 0.95f,
			0.f,
			1.f
			);

	return control * modifyCoef;
}

float AircraftController_Engine::recalculateForceControlWithRestrictions(float control)
{
	if (maxLinearSpeed < 0)
		return control;

	float modifyCoef = 1;
	float forwardVelocity = aircraft->getForwardVelocity();

	//Calculate modifyCoef for slowly decrease control value if the forward velocity is approaching to critical
	if (control > 0)
		modifyCoef = Math::scale<float>(
			forwardVelocity,
			maxLinearSpeed * 0.95f,
			maxLinearSpeed * 1.05f,
			1.f,
			0.f
			);
	else
		modifyCoef = Math::scale<float>(
			forwardVelocity,
			-maxLinearSpeed * 1.05f,
			-maxLinearSpeed * 0.95f,
			0.f,
			1.f
			);

	return control * modifyCoef;
}

AircraftController_Engine::AircraftController_Engine(Aircraft* aircraft, AircraftEngine* engine)
{
	assert(aircraft != nullptr && engine != nullptr);
	this->aircraft = aircraft;
	this->engine = engine;

	//Init PID controllers
	controllerForce = new PID_Limited(10, 0.5, 1);
	controllerForce->maxIntegral = 1;
	controllerMoment = new PID_Limited(5, 0.1, 2);
	controllerMoment->maxIntegral = 1;
}

AircraftController_Engine::~AircraftController_Engine()
{
	delete controllerForce;
	delete controllerMoment;
}

void AircraftController_Engine::update(float dt)
{
	if (dt <= 0 || !active)
		return;

	engine->setControlForce(calculateForceControl(dt));
	engine->setControlMoment(calculateMomentControl(dt));
}

float AircraftController_Engine::calculateMomentControl(float dt)
{
	float targetAngle = calculateTargetAngle();

	controllerMoment->setTarget(0);
	controllerMoment->setState(-targetAngle);

	auto control = controllerMoment->calculateControl(dt);
	
	return recalculateMomentControlWithRestrictions(control);
}

float AircraftController_Engine::calculateForceControl(float dt)
{
	//Calculate target aircraft Speed
	//Angle betwen velocity and target point
	float errorAngle = std::abs(angleBetwenVectors(aircraft->getVelocity(), targetPoint - aircraft->getPoint()));
	//Clamp error angle [0,pi/2] -> [1,0]
	float speedRate = Math::scale<float>(
		errorAngle,
		0.f,
		3.14 / 2,
		1.f,
		0.f
		);
	/*Get target speed as scaled value betwen minFlightSpeed and 1.05*maxFlightSpeed
	* if errorAngle -> 0 then speed increased
	* if errorAngle -> pi/2 then speed deacresed
	*/
	speedRate = pow(speedRate,3);
	float targetSpeed = Math::scale<float>(
		speedRate,
		0.f,
		1.f,
		minFlightSpeed,
		maxFlightSpeed*1.05		
		);

	controllerForce->setTarget(targetSpeed);
	controllerForce->setState(aircraft->getForwardVelocity());
	float control = controllerForce->calculateControl(dt);
	return recalculateForceControlWithRestrictions(control);
}

void AircraftController_Engine::setActive(bool active)
{
	if (!active)
	{
		controllerForce->clearController();
		controllerMoment->clearController();
		engine->setControlForce(0);
		engine->setControlMoment(0);
	}
	this->active = active;

}

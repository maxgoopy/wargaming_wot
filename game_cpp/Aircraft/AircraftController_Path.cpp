#include "AircraftController_Path.h"

void AircraftController_Path::createNewPath(const Vector2& target)
{
	targetPoint = target;
}

Vector2 AircraftController_Path::getTargetPointOnPath(const Vector2& currentPoint)
{
	return targetPoint;
}

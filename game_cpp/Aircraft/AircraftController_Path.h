#pragma once
#include "..\Utils\Vector2.h"

//Class for generating path to the point 
//NOT IMPLEMENTED
//Currently simple save target point and return them
class AircraftController_Path
{
	Vector2 targetPoint;
	float targetAngle;

public:
	void createNewPath(const Vector2& target);
	Vector2 getTargetPointOnPath(const Vector2& currentPoint);
};


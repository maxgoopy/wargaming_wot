#pragma once
#include "Aircraft.h"
#include "../Utils/Vector2.h"

//Simple engine class
class AircraftEngine
{
	typedef Aircraft Craft;

	//Craft fot applying impulse
	Craft *craft;
	
	//Max force for positive controlForce
	float maxPositiveForce;
	//Max force for negative controlForce
	float maxNegativeForce;
	//Max moment for controlMoment
	float maxMoment;

	//Controll force [-1,1]
	float controlForce = 0;

	//Control moment [-1,1]
	float controlMoment = 0;

public:

	/*Efficient forward speed for apply angleMovement.
	* If forward speed lower than that, angular moment will be reduced proportionally
	* If =0 then no restrictions
	*/
	float minEfficientForwardSpeed = 1.f;

private:
	AircraftEngine();

	float CalculateImpulse(float dt);
	float CalculateAngleImpulse(float dt);

public:
	/*
	* @param craft - craft for applying impulse
	* @param maxPositiveForce - Max force for positive controlForce
	* @param maxMoment - Max force for negative controlForce
	* @param maxNegativeForce - Max moment for controlMoment
	*/
	AircraftEngine(Craft* craft, float maxPositiveForce, float maxMoment , float maxNegativeForce = 0);

	//Main update function
	void update(float dt);
	void setControlForce(float control) { controlForce = control; };
	void setControlMoment(float control) { controlMoment = control; };
};


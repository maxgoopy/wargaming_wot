#pragma once
#include "Aircraft.h"
#include "..\Utils\Vector2.h"

//Base class of aerodynamics with simple realisation 
class AircraftAerodynamics
{
	typedef Aircraft Craft;

	//Craft for applying impulse
	Craft* craft;

	//Coef for transfer lateral velocity to forward
	float transferCoef = 20.0f;

	//Coef for dissipation lateral velocity
	float dissipationCoef = 0.01f;

	AircraftAerodynamics();
	Vector2 calculateImpulse(float dt);
	float calculateTransferEfficiency();
public:
	AircraftAerodynamics(Craft* craft);

	//Main update function
	void update(float dt);
};


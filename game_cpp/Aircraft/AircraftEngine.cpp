#include "AircraftEngine.h"
#include <cassert>
#include "..\Utils\Math.h"

float AircraftEngine::CalculateImpulse(float dt)
{
	if (controlForce > 0)
		return dt * controlForce * maxPositiveForce;
	else
		return dt * controlForce * maxNegativeForce;
}

float AircraftEngine::CalculateAngleImpulse(float dt)
{
	float efficient = 1;

	if (minEfficientForwardSpeed != 0)
		float efficient = Math::scale<float>(
			pow(abs(craft->getForwardVelocity()) / minEfficientForwardSpeed, 2),
			0.f,
			1.f,
			0.f,
			1.f
			);

	return dt * controlMoment * efficient * maxMoment ;
}

AircraftEngine::AircraftEngine(Craft * craft, float maxPositiveForce, float maxMoment, float maxNegativeForce)
	: maxPositiveForce(maxPositiveForce), 
	maxMoment(maxMoment), 
	maxNegativeForce(maxNegativeForce)
{
	assert(craft);
	this->craft = craft;
}

void AircraftEngine::update(float dt)
{
	if (dt <= 0)
		return;

	craft->addImpulse(CalculateImpulse(dt)*craft->getForwardVector());
	craft->addAngularImpulse(CalculateAngleImpulse(dt));

}


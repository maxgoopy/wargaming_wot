#pragma once

class Aircraft;
class Vector2;

//Interface for runway
class IRunway
{
public:
	//Reserve runway for given aircraft, return true on success
	virtual bool runwayReserve(Aircraft* aircraft) = 0;

	//Free runway from given aircraft
	virtual void runwayFree(Aircraft* aircraft) = 0;

	//Get landing position
	virtual void runwayLandingPosition(Vector2& outPoint, float& outAngle) = 0;

	//Initialize landing whith given aircraft
	virtual void initLanding(Aircraft* aircraft) = 0;
};

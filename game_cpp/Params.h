#pragma once

//-------------------------------------------------------
//	game parameters
//-------------------------------------------------------

namespace params
{
	namespace ship
	{
		constexpr float LINEAR_SPEED = 0.5f;
		constexpr float ANGULAR_SPEED = 0.5f;
		constexpr int DEFAULT_AIRCRAFTS_COUNT = 5;
		constexpr float AIRCRAFT_RECHARGING_TIME = 5;
	}

	namespace aircraft
	{
		constexpr float LINEAR_SPEED = 2.f;
		constexpr float ANGULAR_SPEED = 2.5f;
		constexpr float FLIGHT_TIME = 25.f;
	}

}

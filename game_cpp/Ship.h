#pragma once
#include <vector>
#include <queue>
#include <list>
#include "../framework/scene.hpp"
#include "../framework/game.hpp"
#include "Aircraft/IRunway.h"
#include "Utils/Vector2.h"

class Aircraft;


//-------------------------------------------------------
//	Simple ship logic
//-------------------------------------------------------

class Ship : public IRunway
{

	//Owned aircrafts
	std::vector<Aircraft*> aircrafts;

	//Aircrafts queue for launching
	std::queue<Aircraft*> launchQueue;

	struct rechargingAircraft
	{
		Aircraft* aircraft;
		float time;
	};

	//Container of recharging aircrafts
	std::list <rechargingAircraft> rechargingAircrafts;


	//Aircraft which reserved runway
	Aircraft* runwayClient = nullptr;

	//Ship mesh
	scene::Mesh* mesh;

	//Ship point on the map
	Vector2 position;

	//Ship angle on the map
	float angle;

	bool input[game::KEY_COUNT];

	//Time for launch aircraft from runway
	float launchingAircraftTime = 3;

	//Time for landing aircraft on runway
	float landingAircraftTime = 1.f;

	//Start point for launching aircraft (End point for landing) in local space
	Vector2 aircraftStartPoint = Vector2(-0.1f, 0.f);

	//End point for launching aircraft (Start point for landing) in local space
	Vector2 aircraftFinPoint = Vector2(0.4f, 0.f);

	//Total recharching time
	float rechargingTime = 0;

	//Some aircraft launching from Runway
	bool launchInProgress = false;

	//Launching progress [0-1]
	float launchingAircraftProgress = 0.f;

	//Some aircraft landing on runway
	bool landingInProgress = false;

	//Landing progress [0-1]
	float landingAircraftProgress = 0.f;

public:
	Ship();

	void init();
	void deinit();

	//Main update function
	void update(float dt);
	void keyPressed(int key);
	void keyReleased(int key);
	void mouseClicked(Vector2 worldPosition, bool isLeftButton);

	/*Runway interface*/
	virtual bool runwayReserve(Aircraft* aircraft) override;
	virtual void runwayFree(Aircraft* aircraft) override;
	virtual void runwayLandingPosition(Vector2& outPoint, float& outAngle) override;
	virtual void initLanding(Aircraft* aircraft) override;
	/*End Runway interface*/

	//Return Aircraft start point in global or local space
	Vector2 getAircraftStartPoint(bool globalSpace = true)const;

	//Return Aircraft end point in global or local space
	Vector2 getAircraftFinPoint(bool globalSpace = true)const;

	//Return normalized vector from start to end aircraft position on runway
	Vector2 getRunwayDirection()const;

private:
	//Aircraft launching logic
	void updateLaunchingAircraft(float dt);

	//Aircraft landing logic
	void updateLandingAircraft(float dt);

	//Aircraft recharging logic
	void updateRechargingAircrafts(float dt);

	//Try launch aircraft from launchQueue
	void tryLaunchAircraft();

	//Get aircraft and initialize launching process
	void initLaunchingAircraft(Aircraft* aircraft);

	/*Try deactivate aircraft controller and physics
	* If control granted return true, else false
	*/
	bool getControlUnderAircraft(Aircraft* aircraft);
};

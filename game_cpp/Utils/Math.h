#pragma once
namespace Math
{
	//Clamp value betwen min and max and return scaled value betwen newMin and newMax 
	template<typename T, typename P >
	inline P scale(const T& value, const T& min, const T& max, const P& newMin, const P& newMax)
	{
		if (value < min)
			return newMin;
		if (value > max)
			return newMax;

		return newMin + (value - min) / (max - min) * (newMax - newMin);
	}
	
	//Clamp value betwen min and max and return scaled value betwen newMin and newMax 
	template<typename T>
	T scale(const T& value, const T& min, const T& max, const T& newMin, const T& newMax)
	{
		return scale<T, T>(value, min, max, newMin, newMax);
	}

}


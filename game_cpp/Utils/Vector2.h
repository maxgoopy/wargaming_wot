#pragma once
#include <cmath>
//-------------------------------------------------------
//	Basic Vector2 class
//-------------------------------------------------------

class Vector2
{
public:
	float x;
	float y;

	static const Vector2 ZERO_VECTOR;
	static const Vector2 ONE_VECTOR;

	Vector2();
	Vector2(float vx, float vy);
	Vector2(Vector2 const &other);

	Vector2& operator+=(const Vector2& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		return *this;
	}

	Vector2& operator*=(const float& rhs)
	{
		x *= rhs;
		y *= rhs;
		return *this;
	}

	Vector2 normalize(float tolerance = 0.0001)const;
	float length()const;
	Vector2 rotate(float angle)const;

	
};


inline Vector2::Vector2() :
	x(0.f),
	y(0.f)
{
}


inline Vector2::Vector2(float vx, float vy) :
	x(vx),
	y(vy)
{
}


inline Vector2::Vector2(Vector2 const &other) :
	x(other.x),
	y(other.y)
{
}

inline Vector2 Vector2::normalize(float tolerance) const
{
	float length = this->length();
	if (length <= tolerance)
		return ZERO_VECTOR;

	return Vector2(x / length, y / length);
}

inline float Vector2::length() const
{
	return pow(pow(x,2) + pow(y,2),0.5);
}

inline Vector2 Vector2::rotate(float angle) const
{
	return Vector2(x*cos(angle) - y*sin(angle), x * sin(angle) + y * cos(angle));
}

inline Vector2 operator + (Vector2 const &left, Vector2 const &right)
{
	return Vector2(left.x + right.x, left.y + right.y);
}


inline Vector2 operator * (float left, Vector2 const &right)
{
	return Vector2(left * right.x, left * right.y);
}


inline Vector2 operator - (const Vector2& in)
{
	return Vector2(-in.x, -in.y);
}

inline Vector2 operator - (Vector2 const &left, Vector2 const &right)
{
	return left + (-right);
}


inline float crossPoduct(const Vector2& first, const Vector2& second)
{
	return first.x * second.y - first.y * second.x;
}

inline float dotProduct(const Vector2& first, const Vector2& second)
{
	return first.x * second.x + first.y * second.y;
}

inline float angleBetwenVectors(const Vector2& first, const Vector2& second)
{
	float cosval = std::fmax(-1, std::fmin(1, dotProduct(first.normalize(), second.normalize())));

	float angle = acos(cosval);
	if (crossPoduct(first, second) < 0)
		angle = -angle;

	return angle;
}
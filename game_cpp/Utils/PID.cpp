#include "PID.h"
#include <cmath>

PID::PID(float Kp, float Ki, float Kd)
	:Kp(Kp),
	Ki(Ki),
	Kd(Kd)
{
	error = { 0,0 };
	control = { 0 };
}

PID::~PID()
{
}

float PID::calculateControl(float dt)
{
	if (dt <= 0)
		return control[0];

	error[1] = error[0];
	error[0] = getError();
	float prop = error[0];
	integral += dt * error[0];
	integral = std::max(-maxIntegral, std::min(maxIntegral, integral));
	float der = (error[0] - error[1]) / dt;
	control[0] = Kp * prop + Ki * integral + Kd * der;
	if (abs(control[0]) >= 1)
	{
		control[0] += 0;
	}
	return control[0];
}

float PID::setStateAndCalculate(float state, float dt)
{
	setState(state);
	return calculateControl(dt);
}

float PID::getError() const
{
	return target-state;
}

void PID::clearController()
{
	error[0] = 0;
	error[1] = 0;
	control[0] = 0;
	integral = 0;
}

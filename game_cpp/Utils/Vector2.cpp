#include "Vector2.h"


const Vector2 Vector2::ZERO_VECTOR = Vector2(0, 0);
const Vector2 Vector2::ONE_VECTOR = Vector2(1, 1);
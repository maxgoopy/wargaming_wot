#pragma once
#include <vector>

//Base PID controller with simple realisation
class PID
{
protected:
	/**Params*/
	float Kp = 0;
	float Ki = 0;
	float Kd = 0;

	/***/
	float state = 0;
	float target = 0;

	/**Variables*/
	std::vector<float> error;
	std::vector<float> control;
	float integral = 0;
public:
	float maxIntegral = 1000;
public:
	PID(float Kp = 0, float Ki = 0, float Kd = 0);
	virtual ~PID();

	virtual float calculateControl(float dt);
	void setState(float state) { this->state = state; };
	void setTarget(float target) { this->target = target; };
	float setStateAndCalculate(float state, float dt);
	float getError()const;
	void clearController();
};


#pragma once
#include "PID.h"

//PID with restriction on min/max control value
class PID_Limited :
    public PID
{
public:

    PID_Limited(float Kp = 0, float Ki = 0, float Kd = 0);

    float controlMax = 1;
    float controlMin = -1;

    float calculateControl(float dt) override;
};


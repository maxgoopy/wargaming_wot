#include "PID_Limited.h"
#include <cmath>

PID_Limited::PID_Limited(float Kp, float Ki, float Kd) : PID(Kp,Ki,Kd)
{
}

float PID_Limited::calculateControl(float dt)
{
	control[0] = std::fmax(controlMin, std::fmin(controlMax, PID::calculateControl(dt)));

    return control[0];
}
